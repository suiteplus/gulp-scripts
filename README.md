# gulp scripts

Separar scripts de gulp de uso comum em um projeto.

package([opts])
---

```javascript
var gscripts = require('gulp-scripts')

gulp.src('./src/*.js')
    .pipe(gscripts.package())
    .pipe(gscripts.addGlobals())
    .pipe(gscripts.addRev())
    .pipe('dist')
```

Empacota um arquivo escrito com o formato de módulos de node-js, concatenando
as dependências.

**Opções**

 - _external: string | string[]_: Executa da API do browserify, `Bundle:external()`.
   Aponte arquivos ou módulos que o browserify não deve incluir no pacote
   se referenciados (para serem incluídos a partir de outro pacote)

- _es6: boolean_: Transforma de es6 pra es5 com o Babel.

addGlobals
---

O pacote gerado é disponibilidado como uma variável global baseada no
nome do arquivo (_spinal-case_ é convertido em _snake_case_).

Dentro do arquivo, o escopo global pode ser referenciado a partir da variável
GLOBALS.

concatGlobals(path, addExports : boolean)
-------------------------------

Concatena dependências no formato do padrão antigo antes do arquivo fonte.

Para identificar quais são as dependências de cada arquivo, procura por anotações
do ESLint `/* global */`, e então busca recursivamente no caminho passado por
arquivos de mesmo nome.

Caso o arquivo final venha a passar pelo empacotamento, a opção `addExports`
lê a tag eslint `/* exported */` e adiciona ao final do arquivo `exports.<variável> = <variável>;`


**Exemplo empacotamento**

```javascript
//arquivo-1.js
exports.numero = 3;
```

```javascript
//node_modules/pacote/index.js
exports = 5;
```

```javascript
//arquivo-2.js
var dependencia = require('./arquivo-1.js');
var cinco = require('pacote');

exports = function Saida() {
    GLOBALS.document.title = 'Título';
    console.log(dependencia.numero);
    console.log(cinco);
}
```

No caso acima, será gerado um arquivo `dist/arquivo-2.js` e neste declarada
a variável global `arquivo_2` que será igual a `Saida()`.

É também adicionado um comentário indicando o hash do commit git atual no momento
da operação.

Ferramentas de teste
---

**runTest({ localPath , destPath, destFilename? , scriptId , noCoverage? }) : Promise**

Teste de scripts de servidor do netsuite.
 
- Escreva seu teste em _localPath_ usando a ferramenta fornecida (mais abaixo); Exporte a função `Tester::run`;

- Configure o nscabinet (nsconfig.json)

- Crie a função no gulpfile sem o id do script; Execute uma vez pra fazer upload do arquivo
  para o netsuite (voltará erro, isto é ok);

- Crie um RESTlet, adicione como script o arquivo recém colocado no netsuite;
  adicione como biblioteca o `dist/test-bundle.js` presente nesse repositório;
  adicione como função post o método `Tester::run` que foi exportado;

- Preencha o ID do restlet criado no gulpfile; Execute novamente.
  
** Ferramenta fornecida **

 - Para acesso, use `require('test-bundle')` dentro do script de teste;
 
 - `test_bundle.chai` expõe uma cópia da biblioteca de assertions "chai";
 
 - `test_bundle.Tester() : Tester` expõe o meio de definir os testes;
 
 - `test_bundle.write(filename, contents)` salva o conteúdo em pasta local após a execução.
   
```javascript
interface Tester {
  //executa um bloco de código imprimindo uma resposta relativa
  //não aceita blocos aninhados
  test : ( description , function ) => Tester
  //apenas executa, sem imprimir nada
  notest : ( function ) => Tester
  run : () => { coverage : any , text : string }
}
```
 
 - `console.log` é sobrescrito pra ser exibido na resposta do teste;
 
Exemplo de código de teste:
 
```javascript
var tbundle = require('test-bundle');
var chai = tbundle.chai;
var meu_modulo = require('../dist/meu-modulo.js');
 
var t = tbundle.Tester().test('Teste 1' , function() {
     
    chai.expect(meu_modulo.um).is.equal(1);
     
}).notest( function() {
  
    setupSomeData();
     
})

module.exports = t.run
```
 
O que retornará algo como:
 
```
  - Teste 1: OK
  (dados de cobertura)
```
... e irá gerar o relatório de cobertura na pasta do projeto.

**O que acontece?**

 - O teste é empacotado, depois instrumentado
 
 - Enviado para o netsuite, substituindo o arquivo principal do script
 
 - O script é chamado, trazendo resultados, logs e o objeto de cobertura do istanbul.
 
 - É gerado o relatório de cobertura
 
OBS: Como `tbundle` é incluído externalmente, ele não conta na cobertura.
Incluir externalmente é uma das formas possíveis de se adicionar bibliotecas sem que elas sejam incluídas
na instrumentação.


** noCoverage = true **

Neste caso, nem a instrumentação é aplicada, nem o relatório gerado, o que deve
tornar a execução mais rápida e permite o debug do código.

** Exemplo gulp **

```javascript
runTest({
    localPath : 'test/teste.js' ,
    destPath : '/SuiteScripts/testes/' ,
    scriptId : 99
}).then( () => console.log('TESTE OK'); );
```